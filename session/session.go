/*
   This file is part of photo_blog.

   photo_blog is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   photo_blog is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with photo_blog.  If not, see <https://www.gnu.org/licenses/>.

*/

package session

import (
	"database/sql"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
)

var DB *sql.DB
var err error

func init() {
	// Open the database
	DB, err = sql.Open("mysql", "root:@tcp(localhost:3306)/users?charset=utf8")
	CheckFatal(err)
	//	defer DB.Close()
}

// users functions
func GetUser(r *http.Request) string {
	var u string
	// find sid
	cookie, err := r.Cookie("SID")
	if err != nil {
		if err == http.ErrNoCookie {
			return u
		}
		log.Fatal(err)
	}

	// find user from session table in sql
	rows, err := DB.Query(`SELECT uname FROM session WHERE sid="` + cookie.Value + `";`)
	CheckFatal(err)
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&u)
		CheckFatal(err)
		u = u
	}
	return u
}

func Loged(r *http.Request) bool {
	// find if the cookie SID exists
	cookie, err := r.Cookie("SID")
	if err != nil {
		return false
	}
	// find if the record exists
	rows, err := DB.Query(`SELECT uname FROM session WHERE sid="` + cookie.Value + `";`)
	defer rows.Close()
	if err != nil {
		return false
	}
	return true
}

func UserEx(u string) bool {
	var value string
	rows, err := DB.Query(`SELECT uName FROM users WHERE uName="` + u + `";`)
	if err != nil {
		return false
	}
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&value)
		CheckFatal(err)
		value = value
	}
	if len(value) < 1 {
		return false
	}
	return true
}

func CheckFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
