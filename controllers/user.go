/*
   This file is part of photo_blog.

   photo_blog is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   photo_blog is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with photo_blog.  If not, see <https://www.gnu.org/licenses/>.

*/

package controllers

import (
	"crypto/sha1"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	"photo_blog/session"

	uuid "github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
)

var tpl *template.Template
var goLogin = `  <script type="text/javascript">
      setTimeout("location.href = '/';",3000);
 </script>`
var goSign = `  <script type="text/javascript">
      setTimeout("location.href = '/signup';",3000);
 </script>`

func init() {
	tpl = template.Must(template.ParseGlob("templates/*"))
}

// index ...
func Index(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		if !session.UserEx(r.FormValue("uname")) {
			w.Header().Set("Content-Type", "text/html; charset=utf-8")
			fmt.Fprint(w, "Username not found"+goLogin)
			return
		}

		//check password
		rows, err := session.DB.Query(`SELECT pass FROM users WHERE uName="` + r.FormValue("uname") + `";`)
		CheckFatal(err)
		defer rows.Close()
		var pass string
		for rows.Next() {
			err := rows.Scan(&pass)
			CheckFatal(err)
		}
		psw := []byte(pass)

		err = bcrypt.CompareHashAndPassword(psw, []byte(r.FormValue("psw")))
		if err != nil {
			w.Header().Set("Content-Type", "text/html; charset=utf-8")
			fmt.Fprint(w, "Username and/or password do not match"+goLogin)
			return
		}

		// create cookie
		id, err := uuid.NewV4()
		CheckFatal(err)
		http.SetCookie(w, &http.Cookie{
			Name:  "SID",
			Value: id.String(),
		})

		// insert login session
		stmtS, err := session.DB.Prepare(`INSERT INTO session(sid,uname) VALUES ("` + id.String() + `","` + r.FormValue("uname") + `");`)
		CheckFatal(err)
		defer stmtS.Close()
		_, err = stmtS.Exec()
		CheckFatal(err)

		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	if session.Loged(r) {
		http.Redirect(w, r, "/account", http.StatusSeeOther)
	} else {
		tpl.ExecuteTemplate(w, "index.gohtml", nil)
	}

}

func Logout(w http.ResponseWriter, r *http.Request) {
	if session.Loged(r) {
		// delete record
		stmt, err := session.DB.Prepare(`DELETE FROM session WHERE uname="` + session.GetUser(r) + `";`)
		CheckFatal(err)
		defer stmt.Close()
		_, err = stmt.Exec()
		CheckFatal(err)
		// delete cookie
		http.SetCookie(w, &http.Cookie{
			Name:   "SID",
			MaxAge: -1,
		})
	}
	http.Redirect(w, r, "/", http.StatusSeeOther)
	return

}

func Signup(w http.ResponseWriter, r *http.Request) {
	// check if logged in
	if session.Loged(r) {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	// posting a sign up
	if r.Method == http.MethodPost {
		if session.UserEx(r.FormValue("uname")) {
			w.Header().Set("Content-Type", "text/html; charset=utf-8")
			fmt.Fprint(w, "Username: "+r.FormValue("uname")+" already taken."+goSign)
			return
		}

		//encrypt pass
		bs, err := bcrypt.GenerateFromPassword([]byte(r.FormValue("psw")), bcrypt.MinCost)
		CheckFatal(err)

		// insert user to db
		sqlQ := `INSERT INTO users(email,uName,fName,lName,pass) VALUES ("`
		sqlQ += r.FormValue("email")
		sqlQ += `","`
		sqlQ += r.FormValue("uname")
		sqlQ += `","`
		sqlQ += r.FormValue("fname")
		sqlQ += `","`
		sqlQ += r.FormValue("lname")
		sqlQ += `","`
		sqlQ += string([]byte(bs))
		sqlQ += `");`
		stmt, err := session.DB.Prepare(sqlQ)
		CheckFatal(err)
		defer stmt.Close()
		_, err = stmt.Exec()
		CheckFatal(err)

		// create uuid for the cookie
		id, err := uuid.NewV4()
		CheckFatal(err)

		// create cookie
		http.SetCookie(w, &http.Cookie{
			Name:  "SID",
			Value: id.String(),
		})

		//insert login session
		stmtS, err := session.DB.Prepare(`INSERT INTO session(sid,uname) VALUES ("` + id.String() + `","` + r.FormValue("uname") + `");`)
		CheckFatal(err)
		defer stmtS.Close()
		_, err = stmtS.Exec()
		CheckFatal(err)

		//redirect
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}
	tpl.ExecuteTemplate(w, "signup.gohtml", nil)
}

// photo section
func Account(w http.ResponseWriter, r *http.Request) {
	if !session.Loged(r) {
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		fmt.Fprint(w, "Login first!"+goLogin)
		return
	}
	// process form submission
	if r.Method == http.MethodPost {
		if r.FormValue("method") == "POST" {
			mf, fh, err := r.FormFile("nf")
			CheckFatal(err)
			defer mf.Close()
			// create sha for file name
			ext := strings.Split(fh.Filename, ".")[1]
			h := sha1.New()
			io.Copy(h, mf)
			fname := fmt.Sprintf("%x", h.Sum(nil)) + "." + ext
			// insert name to the database
			stmtS, err := session.DB.Prepare(`INSERT INTO images(uname,img) VALUES ("` + session.GetUser(r) + `","` + fname + `");`)
			CheckFatal(err)
			defer stmtS.Close()
			_, err = stmtS.Exec()
			CheckFatal(err)

			// check if direcotory exists or create
			if _, err := os.Stat("public"); os.IsNotExist(err) {
				os.Mkdir("public", 0700)
			}
			if _, err := os.Stat("public/pics"); os.IsNotExist(err) {
				os.Mkdir("public/pics", 0700)
			}

			// create new file
			wd, err := os.Getwd()
			CheckFatal(err)
			path := filepath.Join(wd, "public", "pics", fname)
			nf, err := os.Create(path)
			CheckFatal(err)
			defer nf.Close()
			// copy
			mf.Seek(0, 0)
			io.Copy(nf, mf)
		} else if r.FormValue("method") == "DELETE" {
			// delete img from db
			stmt, err := session.DB.Prepare(`DELETE FROM images WHERE uname="` + session.GetUser(r) + `" AND img="` + r.FormValue("img") + `";`)
			CheckFatal(err)
			defer stmt.Close()
			_, err = stmt.Exec()
			CheckFatal(err)
			// delete img from filesystem
			if _, err := os.Stat("public/pics/" + r.FormValue("img")); !os.IsNotExist(err) {
				err = os.Remove("public/pics/" + r.FormValue("img"))
				CheckFatal(err)
			}
			// redirect
			http.Redirect(w, r, "/", http.StatusSeeOther)
			return
		}
	}

	//get images from database
	var img, s string
	rows, err := session.DB.Query(`SELECT img FROM images WHERE uname="` + session.GetUser(r) + `";`)
	CheckFatal(err)
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&s)
		CheckFatal(err)
		img += "|" + s
	}
	xs := strings.Split(img, "|")
	//	if len(xs) > 1 {
	tpl.ExecuteTemplate(w, "account.gohtml", xs[1:])
	//	} else {
	//		tpl.ExecuteTemplate(w, "account.gohtml", nil)
	//	}

}

// log error func

func CheckFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
