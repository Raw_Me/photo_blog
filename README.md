[![pipeline status](https://gitlab.com/Raw_Me/photo_blog/badges/master/pipeline.svg)](https://gitlab.com/Raw_Me/photo_blog/-/commits/master)

# Table of Contents

1.  [Photo Blog](#photo-blog)
    1.  [General](#general)
    1.  [Build](#build)
    1.  [Docker Build](#docker-build)
    1.  [Docker Run](#docker-run)
    1.  [Run the application](#run-the-application)
    1.  [Terraform AWS Quick Run](#terraform-aws-quick-run)
    1.  [License](#license)


# Photo Blog


#### General

This project is a simple photo blog to practice my Golang skills, and serve as an easy to deploy app for more complex POCs. The basic idea is to sign in and upload photos.


#### Build

-   To build this project you need to first edit session/session.go line 16 :
    ```
    DB, err = sql.Open("mysql", "username:password@tcp(127.0.0.1:3306)/users?charset=utf8")
    ```
-   Then run:
    ```
    go build -o photo_blog
    ```


#### Docker Build
-   To build a docker image:
    ```
    docker build -t photo_blog .
    ```


#### Docker Run
-   You can run the container you built locally or just use the latest version from Docker Hub:


    ```
    sudo docker run -d -p 8080:8080 --network="host" rawme/photo_blog:latest
    ```
    
    
#### Run the application

-   First you need to have the MySQL db ready:
    ```
    mysql -u username -p users < db.sql
    ```
    Note: the db name is users. 

-   Then you can run the executable in case you built the app or just use:
    ```
    go run main.go
    ```

#### Terraform AWS Quick Run

-   You can use this Terraform file to quickly run the application using AWS

    
    You just need to add:
    
    +   The name of your ssh key, line 10.
    +   The security groups [Note: it must allow port 8080 incoming], line 13.
    +   The private key file, line 29.
     
     
     
    ```
    terraform init
    ```
    ```
    terraform apply
    ```
    Preferably: Use (AWS CLI).

#### License

GPL v3 - <https://www.gnu.org/licenses/gpl-3.0.en.html>

