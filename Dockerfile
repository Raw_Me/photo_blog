FROM golang:1.14

WORKDIR /go/src/photo_blog
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...

CMD ["photo_blog"]