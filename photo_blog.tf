/*
   This file is part of photo_blog.

   photo_blog is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   photo_blog is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with photo_blog.  If not, see <https://www.gnu.org/licenses/>.

*/

provider "aws" {
  profile = "default"
}

resource "aws_instance" "Photo_Blog" {
  ami = "ami-000e7ce4dd68e7a11"
  instance_type = "t2.micro"
  // Add your ssh keyname here
  key_name = ""
  // Add your security groups 
  security_groups = [
    "",
  ]
  tags = {
    Name = "Terraform_Photo_Blog"
  }

  // copy the db schema to the server
  provisioner "file" {
    source      = "db.sql"
    destination = "/home/centos/db.sql"
  }
  
  connection {
    type     = "ssh"
    user     = "centos"
    // Add the private key file here
    private_key = file("")
    host        = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum install -y yum-utils",
      "sudo dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo",
      "sudo dnf install docker-ce --nobest -y",
      "sudo systemctl start docker",
      "sudo systemctl enable docker",
      "sudo docker run -d --name mariadb -e MYSQL_ALLOW_EMPTY_PASSWORD=\"yes\" --network=\"host\" mariadb:latest",
      "sleep 10",
      "sudo docker exec -i mariadb sh -c 'exec mysql -u root' < /home/centos/db.sql",
      "sudo docker run -d --name photo_blog -p 8080:8080 --network=\"host\" rawme/photo_blog:latest",
    ]
  }
}
