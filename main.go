/*
   This file is part of photo_blog.

   photo_blog is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   photo_blog is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with photo_blog.  If not, see <https://www.gnu.org/licenses/>.

*/

package main

import (
	"log"
	"net/http"
	"photo_blog/controllers"
)

func main() {
	http.HandleFunc("/", controllers.Index)
	http.HandleFunc("/signup", controllers.Signup)
	http.HandleFunc("/account", controllers.Account)
	http.HandleFunc("/logout", controllers.Logout)
	// add route to serve pictures
	http.Handle("/public/", http.StripPrefix("/public", http.FileServer(http.Dir("./public"))))
	http.HandleFunc("/favicon.ico", faviconHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "imgs/favicon-16x16.png")
}
